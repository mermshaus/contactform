Download PHPMailer v5.1 from [GitHub][gh] and move it into the `./library`
directory so that `class.phpmailer.php` resides at
`./library/phpmailer/PHPMailer_v5.1/class.phpmailer.php`.

If you want to use a different version of PHPMailer, you will need to adjust the
path in `./library/org/example/Contact/ContactModel.php`.



  [gh]: https://github.com/PHPMailer/PHPMailer/releases
